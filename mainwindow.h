#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImageCapture>
#include <QMediaCaptureSession>
#include <QGraphicsVideoItem>
//#include <QCameraViewfinder>
#include <QCamera>
#include <QMediaDevices>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }

class QMediaCaptureSession;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();



private slots:
    void on_pushButton_clicked();
    void doCamera();

    void on_pushButton_2_clicked();

    void on_captureButton_clicked();

    void on_pushButton_3_clicked();

    void on_camerasComboBox_textActivated(const QString &arg1);

private:
    Ui::MainWindow *ui;

    QImageCapture *imageCapture;
    QGraphicsVideoItem *item;
  //  QCamera *camera;
    QScopedPointer<QCamera> m_camera;
    // QCameraViewfinder   *view_finder;
    QMediaCaptureSession *captureSession;
    QScopedPointer<QAudioInput> m_audioInput;
    QMediaDevices *mediaDevices;
};

QT_END_NAMESPACE
#endif // MAINWINDOW_H
