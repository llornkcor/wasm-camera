#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QAudioInput>
#include <QCamera>
#include <QCameraDevice>
#include <QGraphicsScene>
#include <QGraphicsVideoItem>
#include <QImageCapture>
#include <QMediaCaptureSession>
#include <QMediaDevices>
#include <QTimer>
#include <QVideoWidget>
#include <QLabel>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      imageCapture(nullptr)
{
    ui->setupUi(this);
    captureSession = new QMediaCaptureSession;

    mediaDevices = new QMediaDevices;
    connect(mediaDevices, &QMediaDevices::videoInputsChanged,
            [=]() { doCamera(); });

    // on wasm, these are not ready yet
    const QList<QCameraDevice> cameras = QMediaDevices::videoInputs();

    for (const QCameraDevice &cameraDevice : cameras) {
        qWarning() << Q_FUNC_INFO << cameraDevice.description();

        ui->camerasComboBox->addItem(cameraDevice.description(), cameraDevice.id());
    }
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::doCamera()
{
    m_audioInput.reset(new QAudioInput);
    captureSession->setAudioInput(m_audioInput.get());

    const QList<QCameraDevice> cameras = QMediaDevices::videoInputs();
    qWarning() << Q_FUNC_INFO << "<<<<<<<<<<<<<<<<<<<<<<<cameras.count" << cameras.count();

    QCameraDevice defaultCamera = QMediaDevices::defaultVideoInput();

    qWarning()<< "defaultCamera description"<< defaultCamera.description();

    for (const QCameraDevice &cameraDevice : cameras) {

        qWarning() << Q_FUNC_INFO << cameraDevice.description()
                   << ui->camerasComboBox->findText(cameraDevice.description());

        if (ui->camerasComboBox->findText(cameraDevice.description()) == -1)
            ui->camerasComboBox->addItem(cameraDevice.description(), cameraDevice.id());
//        m_camera.reset(new QCamera(cameraDevice));
    }

    if (cameras.count() == 0) {
        qWarning() << "No camera found";
    } else {

//        m_camera.reset(new QCamera(QMediaDevices::defaultVideoInput()));

        //    camera = new QCamera(QMediaDevices::defaultVideoInput());

//         qWarning() << Q_FUNC_INFO << "setting camera:" << m_camera.data()->description();
//        captureSession->setCamera(m_camera.data());

        //    QVideoWidget *viewfinder = new QVideoWidget(); // not gonna work, buddy
        //    viewfinder->show();
        //    captureSession.setVideoOutput(viewfinder);

        item = new QGraphicsVideoItem();
        qWarning() << item;
        QGraphicsScene *scene = new QGraphicsScene(this);
        captureSession->setVideoOutput(item); // sets videoSink

        ui->graphicsView->setScene(scene);
        ui->graphicsView->scene()->addItem(item);
        ui->graphicsView->show();
    }
}

void MainWindow::on_pushButton_clicked()
{
    qWarning() << Q_FUNC_INFO << "cameraFormat null" << m_camera.data()->cameraFormat().isNull();

    //    if (!m_camera.data()->cameraFormat().isNull()) {

    //        auto formats = m_camera.data()->cameraDevice().videoFormats();
    //        qWarning() << Q_FUNC_INFO << formats.count();

    //        if (!formats.isEmpty()) {
    //            // Choose a decent camera format: Maximum resolution at at least 30 FPS
    //            // we use 29 FPS to compare against as some cameras report 29.97 FPS...
    //            QCameraFormat bestFormat;
    //            for (const auto &fmt : formats) {
    //                if (bestFormat.maxFrameRate() < 29 && fmt.maxFrameRate() > bestFormat.maxFrameRate())
    //                    bestFormat = fmt;
    //                else if (bestFormat.maxFrameRate() == fmt.maxFrameRate() &&
    //                         bestFormat.resolution().width()*bestFormat.resolution().height() <
    //                         fmt.resolution().width()*fmt.resolution().height())
    //                    bestFormat = fmt;
    //            }

    //            m_camera->setCameraFormat(bestFormat);
    //            //   m_mediaRecorder->setVideoFrameRate(bestFormat.maxFrameRate());
    //        }
    //    }

    m_camera.data()->start();
}

void MainWindow::on_pushButton_2_clicked()
{
    m_camera.data()->stop();
}

void MainWindow::on_captureButton_clicked()
{
    qWarning() << Q_FUNC_INFO << "take photo" << m_camera.data();

    //       connect(m_camera.data(), &QCamera::activeChanged, this, &Camera::updateCameraActive);

    connect(m_camera.data(), &QCamera::errorOccurred,
            [=](QCamera::Error error, const QString &errorString) {
        qWarning() << "Error occurred" << errorString;
    });

    QImageCapture *m_imageCapture = new QImageCapture;

    qDebug() << Q_FUNC_INFO << m_imageCapture;

    connect(m_imageCapture, &QImageCapture::readyForCaptureChanged, [=] (bool ready) {

        qWarning() << "MainWindow::readyForCaptureChanged" << ready;
    });

    connect(m_imageCapture,
            &QImageCapture::imageCaptured,
            [=] (int id, const QImage &preview) {
        qWarning() << "MainWindow::imageCaptured" << id << preview;

    });

    connect(m_imageCapture, &QImageCapture::imageSaved,
            [=] (int id, const QString &fileName) {
        qWarning() << "MainWindow::imageSaved" << id << fileName;
        QFileInfo fi(fileName);
        if (!fi.exists()) {
            qWarning() << fileName << "Does not exist";
        } else {
            QDialog *dlg = new QDialog(this);
            dlg->setWindowTitle(fi.fileName());
            QHBoxLayout *l = new QHBoxLayout(dlg);
            QLabel *label = new QLabel;
            l->addWidget(label);
            label->setPixmap(QPixmap(fileName));
            dlg->show();
        }

    });
    connect(m_imageCapture, &QImageCapture::imageExposed,
            [=] (int id) {
        qWarning() << "MainWindow::imageExposed" << id;

    });

    connect(m_imageCapture, &QImageCapture::imageAvailable,
            [=] (int id, const QVideoFrame &frame) {
        qWarning() << "MainWindow::imageAvailable" << id << frame;

    });
    connect(m_imageCapture,
            &QImageCapture::errorOccurred, [=]
            (int id, QImageCapture::Error error, const QString &errorString) {
        qWarning() << "MainWindow::errorOccurred" << id << error << errorString;
    });

    captureSession->setImageCapture(m_imageCapture);

    // take photo
    if (m_imageCapture->isReadyForCapture())
        m_imageCapture->captureToFile(QStringLiteral("/home/web_user/image.png"));

    //  m_imageCapture->capture(); // buffer

}

void MainWindow::on_pushButton_3_clicked()
{
    // open
    QFileDialog *dialog = new QFileDialog(this);
    //    dialog->setViewMode(QFileDialog::Detail);
    dialog->setNameFilter(tr("Images (*.png *.xpm *.jpg)"));
    //    dialog->setFileMode(QFileDialog::AnyFile);
    dialog->show();
}


void MainWindow::on_camerasComboBox_textActivated(const QString &arg1)
{
    qWarning() << Q_FUNC_INFO << arg1;

    const QList<QCameraDevice> cameras = QMediaDevices::videoInputs();

    qWarning() << Q_FUNC_INFO << __LINE__ ;
    qWarning() << Q_FUNC_INFO << cameras.count();
    for (const QCameraDevice &cameraDevice :cameras) {

        qWarning() << Q_FUNC_INFO << cameraDevice.description();

        if (arg1 == cameraDevice.description()) {
            qWarning() << Q_FUNC_INFO << "setting camera:" << cameraDevice.description();
            m_camera.reset(new QCamera(cameraDevice));
            captureSession->setCamera(m_camera.data());
            break;
        }
    }
}

